var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

function validate(monthGiven, dayGiven){
  var date = parseFloat(document.getElementById(dayGiven).value);
  var month = parseFloat(document.getElementById(monthGiven).value);
  var check = checkDateOrder();
  if ((month == 1 && date > 28) || (date < 1 || date > 31) || (date == 31 && monthDays[month] !=31) ){
    document.getElementById(dayGiven).style.color = "#FFF";
    document.getElementById(dayGiven).style.backgroundColor = "#A00";
    return false;
  }
  else{
    document.getElementById(dayGiven).style.color = "#000";
    document.getElementById(dayGiven).style.backgroundColor = "#FFF";
    if (check == true){
      return true;
    }
    return false;
  }
}

function checkDateOrder(){
  var month1 = document.getElementById("month1").value;
  var month2 = document.getElementById("month2").value;
  var day1 = document.getElementById("day1").value;
  var day2 = document.getElementById("day2").value;
  if ( (parseFloat(month1) > parseFloat(month2)) || (parseFloat(month1) == parseFloat(month2) && (parseFloat(day1) >= parseFloat(day2))) ) {
    document.getElementById("calculatedDate").innerHTML = "Study cannot be after or on Test Day";
    return false;
  }
 else{
   document.getElementById("calculatedDate").innerHTML = " ";
   return true;
 }
}

function calculateDate(){
  if (validate("month1", "day1") && validate("month2","day2")){
    var month1 = parseFloat(document.getElementById("month1").value)
    var month2 = parseFloat(document.getElementById("month2").value);
    var day1 = parseFloat(document.getElementById("day1").value);
    var day2 = parseFloat(document.getElementById("day2").value);
    var dayInterval = 0;
    var recDay = day1;
    var recMonth = month1;
    var days = 0;
    var timeString = "";
    if (month1 != month2){
      days = monthDays[month1] - day1;
    }
    for (i=month1+1; i<month2; i++){
      days += monthDays[i];
    }
    days += day2;
    dayInterval = days*0.2;
    while (dayInterval >= 1){
      recDay ++;
      if (recDay > monthDays[recMonth]){
        recMonth ++;
        recDay = 1;
      }
      dayInterval --;
    }
    dayInterval = Math.round(dayInterval*24);
    if (dayInterval > 0 && days < 10){
      timeString = ", " + dayInterval.toString() + " hours after initial study hour.";
    }
    document.getElementById("calculatedDate").innerHTML = "<b>" + monthNames[recMonth] + " " + recDay.toString() + "</b>" + timeString;
  }
  else{
    document.getElementById("calculatedDate").innerHTML = "Please Fix Input";
  }
}
